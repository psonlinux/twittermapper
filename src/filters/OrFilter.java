package filters;

import java.util.ArrayList;
import java.util.List;

import twitter4j.Status;

public class OrFilter implements Filter{
	private final Filter child1, child2;
	
	public OrFilter(Filter child1, Filter child2) {
		this.child1 = child1;
		this.child2 = child2;
	}
	
	@Override
	public boolean matches(Status s) {
		// TODO Auto-generated method stub
		return child1.matches(s) || child2.matches(s);
	}

	@Override
	public List<String> terms() {
		// TODO Auto-generated method stub
		List<String> ans = new ArrayList<String>();
		
		List<String> c1 = child1.terms();
		for(String s: c1) {
			ans.add(s);
		}
		
		List<String> c2 = child2.terms();
		for(String s: c2) {
			ans.add(s);
		}
		return ans;
	}

	@Override
	public String toString() {
		return "(" + child1.toString() + " or " + child2.toString() + ")";
	}
}
