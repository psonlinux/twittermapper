package twitter;

import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Encapsulates the connection to Twitter
 *
 * Terms to include in the returned tweets can be set with setFilterTerms
 *
 * Implements Observable - each received tweet is signalled to all observers
 */
public class LiveTwitterSource extends TwitterSource {
    private TwitterStream twitterStream;
    private StatusListener listener;

    public LiveTwitterSource() {
        initializeTwitterStream();
    }

    @Override
	protected void sync() {
        FilterQuery filter = new FilterQuery();
        // https://stackoverflow.com/questions/21383345/using-multiple-threads-to-get-data-from-twitter-using-twitter4j
        String[] queriesArray = terms.toArray(new String[0]);
        filter.track(queriesArray);

        System.out.println("Syncing live Twitter stream with " + terms);

        twitterStream.filter(filter);
    }

    private void initializeListener() {
        listener = new StatusAdapter() {
            @Override
            public void onStatus(Status status) {
                // This method is called each time a tweet is delivered by the twitter API
                if (status.getPlace() != null) {
                    handleTweet(status);
                }
           }
        };
    }

    // Create ConfigurationBuilder and pass in necessary credentials to authorize properly, then create TwitterStream.
    // api key : PFLqaZfTJsnN1AmvkU5oD0o8w
    // api key secret : Y347EK8vUDwzz93uMnQ34hOrC1RnuApHfAVNstUPhvdqHtXWer
    // Bearer token : AAAAAAAAAAAAAAAAAAAAANwAGgEAAAAAXfSEjtDFEifR9145I3NhXgq4uKg%3DQEJ2L8Jf6d7UVCcyX8vPeLp3SbkeYKctMOGauxmZC6qII7NdHa
    private void initializeTwitterStream() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setOAuthConsumerKey("PFLqaZfTJsnN1AmvkU5oD0o8w")
                .setOAuthConsumerSecret("Y347EK8vUDwzz93uMnQ34hOrC1RnuApHfAVNstUPhvdqHtXWer")
                .setOAuthAccessToken("849475091188654083-NOm3FQBOu4kVlF75IFErqiaeNHMrja3")
                .setOAuthAccessTokenSecret("b7g5mJZI7iwlegtWHcdRMenkNDgRvSu97TPteTE4ZG0Ei");

        // Pass the ConfigurationBuilder in when constructing TwitterStreamFactory.
        twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
        initializeListener();
        twitterStream.addListener(listener);
    }
}
