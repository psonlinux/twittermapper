module twitterMapper {
	requires org.junit.jupiter.api;
	requires twitter4j;
	requires java.desktop;
	requires JMapViewer;
	requires junit;
}